#include "dstring.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

//-----------------------------------------------------------------------------
/* Dynamicly allocates memory for an array and copies the string str to that array, and then returning that array */
DString dstring_initialize(const char* str)
{
    assert(str != NULL);

    DString a = (DString)malloc(strlen(str) + 1);

    if(a != NULL)
    {
        strcpy(a, str);

        if (strcmp(a, str) == 0)
        {
            return a;
        }
    }

    return NULL;
}
//-----------------------------------------------------------------------------
/* Adds the string from source to the destination one, merging the two at the destination */
int dstring_concatenate(DString* destination, DString source)
{
    DString temp;

    assert(destination != NULL);
    assert(*destination != NULL);
    assert(source != NULL);

    temp = *destination;        //temp serves no purpose??

    temp = (DString)realloc(*destination, strlen(*destination)+strlen(source) + 1);
    if (temp != NULL)
    {
        *destination = temp;
        strncat(*destination, source, strlen(source));
        return 1;
    }

    return -1;

    // Postcondition: *destination innehaller den gamla strangen ihopslagen med source - beh�ver inte testas med assert.
}
//-----------------------------------------------------------------------------
/* Cuts the size of the string in destination down to the amount given by truncatedLength, aka reducing the string length to truncatedLength */
void dstring_truncate(DString* destination, unsigned int truncatedLength)
{
    assert(destination != NULL);
    assert(*destination != NULL);
    assert(truncatedLength >= 0);

    if (strlen(*destination) >= truncatedLength)
    {
        *destination = (DString)realloc(*destination, (truncatedLength + 1));
        *(*destination + truncatedLength) = 0;
    }

    assert(strlen(*destination) == truncatedLength);
}
//-----------------------------------------------------------------------------
/* Prints the string */
void dstring_print(DString str, FILE* textfile)
{
    assert(textfile != NULL);

    if(textfile != NULL)
    {
        fprintf(textfile, "%s\n", str);
    }
}
//-----------------------------------------------------------------------------
/* Deletes a string */
void dstring_delete(DString* stringToDelete)
{
    assert(stringToDelete != NULL);
    assert(*stringToDelete != NULL);

    free(*stringToDelete);
    *stringToDelete = NULL;
}
