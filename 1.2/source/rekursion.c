#include <stdio.h>
#include <assert.h>

//-----------------------------------------------------------------------------
/* Recursion of adding all numbers n down to 0*/
int sum(int n)
{
	int i;

	printf("\nsum(%d) anropas", n);

	if (n < 1)
	{
		return 0;
	}

	else
	{
		i = n+sum(n-1);
		printf("\nsum(%d) returnerar %d", n, i);
		return i;
	}
}
//-----------------------------------------------------------------------------
int main (int argc, char** argv)
{
	int user;
    
    #ifdef DEBUG
	assert(sum(1) == 1);
	assert(sum(2) == 3);
	assert(sum(3) == 6);
	assert(sum(4) == 10);
	assert(sum(5) == 15);
	assert(sum(20) == 210);
	assert(sum(0) == 0);
	assert(sum(-1) == 0);
    #endif

	printf("\nenter a number greater than 0: ");
	scanf("%d", &user);

	if (user > 0)
	{
		sum(user);
        printf("\n");
	}

	return 0;
}
